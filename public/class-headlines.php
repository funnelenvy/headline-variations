<?php

require_once KS_HEADLINES_PLUGIN_PUBLIC_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-headlines-db.php';
require_once KS_HEADLINES_PLUGIN_PUBLIC_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-base62.php';
require_once KS_HEADLINES_PLUGIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-headlines-rest.php';
require_once KS_HEADLINES_PLUGIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-headlines-session.php';

/**
 * @package     Plugin_Name
 */
class Headlines
{
    /**
     * Version used for stylesheet and Javascript assets.
     */
    const VERSION = KS_HEADLINES_EDD_VERSION;

    protected $plugin_slug = 'headlines';

    protected $variations = null;

    protected $script_queue = array();

    protected $sign_verifier = null;

    protected $can_cache = true;

    protected $bot_check_result = null;

    /**
     * Instance of this class.
     */
    protected static $instance = null;

    private function __construct()
    {
        if (!is_admin()) {
			//checks if you have w3 cache plugin
            if (function_exists('w3tc_add_action')) {
                error_log('added cb');
                w3tc_add_action('w3tc_pgcache_cache_key', array($this, 'get_w3tc_cache_key'));
            }
			//including extra short codes and adding new hooks for the functions below
            add_filter('wpseo_replacements', array($this, 'wpseo_replacements'), 10);
            add_filter('the_title', array($this, 'get_post_title'), 10, 2);
            add_filter('single_post_title', array($this, 'single_post_title'), 10, 2);
            add_filter('post_link', array($this, 'post_link'), 10, 3);
            add_filter('page_link', array($this, 'page_link'), 10, 3);

            add_filter('w3tc_can_cache', array($this, 'check_w3tc_can_cache'));

            add_action('template_redirect', array($this, 'check_post_variation'));

            add_action('init', array($this, 'start_session'), 1);

            add_action('wp_footer', array($this, 'append_script_queue'));

            add_filter('query_vars', array($this, 'query_vars'));
            add_action('parse_request', array($this, 'check_parse_request'));
        }
    }

    /**
     * Returns an instance of this class.
     *
     * @return  object    A single instance of this class.
     */
    public static function get_instance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
	//on activate this will run class-headlines-db.php
    public static function activate()
    {
        Headlines_DB::check_database_version();
    }
	//on reactive destroy db, but it's commented out anyway
    public static function deactivate()
    {
        //Headlines_DB::destroy();
    }
	//returns plugin slug
    public function get_plugin_slug()
    {
        return $this->plugin_slug;
    }

    /**
     * Load the plugin text domain for translation.
     */
    public function load_text_domain()
    {
        $domain = $this->plugin_slug;
        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, trailingslashit(WP_LANG_DIR) . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, false, basename(plugin_dir_path(dirname(__FILE__))) . '/languages/');
    }

    /**
     * Register and enqueue the public-facing stylesheet.
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_slug . '-plugin-styles', plugins_url('assets/css/public.css', __FILE__), array(), self::VERSION);
    }

    /**
     * Register and enqueue public-facing Javascript files.
     */
    public function enqueue_scripts()
    {
        wp_enqueue_scripts($this->plugin_slug . '-plugin-script', plugins_url('assets/js/public.js', __FILE), array('jquery'), self::VERSION);
    }
	//checks for w3tc if it's there, if it is, it tells w3tc to not cache
    public function check_w3tc_can_cache($can_cache)
    {
        return $this->can_cache ? $can_cache : false;
    }
	
	//creates custom tags for the headlines
    public function wpseo_replacements($replacements)
    {
        if (!isset($replacements['%%id%%']) || $this->is_bot() || is_preview() || is_feed()) {
            return $replacements;
        }

        $post_id = $replacements['%%id%%'];
        if (!in_array(get_post_type($post_id), array('post', 'page'))) {
            return $replacements;
        }

        $variation = $this->get_post_variation($post_id);
        if ($variation) {
            $variation = Headlines_DB::get_post_variation($variation['v']);
        }
        if ($variation) {
            $replacements['%%title%%'] = $variation->post_title;
            $this->can_cache = false;
        }

        return $replacements;
    }

    /**
     * Override the post title.
     */
    public function get_post_title($title, $id)
    {
        if ($this->is_bot() || is_feed() || is_preview() || !in_array(get_post_type($id), array('post', 'page'))) {
            return $title;
        }

        $post = get_post($id);
        if (!$post) {
            return $title;
        }

        remove_filter('the_title', array($this, 'get_post_title'), 10);

        $variation = $this->get_post_variation($post->ID);
        if ($variation) {
            $variation = Headlines_DB::get_post_variation($variation['v']);
        }

        if ($variation) {
            $title = $variation->post_title;
            $this->can_cache = false;
        }

        $title = apply_filters('the_title', $title, $id);

        add_filter('the_title', array($this, 'get_post_title'), 10, 2);

        return $title;
    }

    /**
     * Override the single post title (<title />).
     */
    public function single_post_title($post_title, $post)
    {
        if ($this->is_bot() || is_preview() || is_feed() || !in_array(get_post_type($post), array('post', 'page'))) {
            return $post_title;
        }

        $post = get_post($post);
        if (!$post) {
            return $post_title;
        }

        $variation = $this->get_post_variation($post->ID);
        if ($variation) {
            $variation = Headlines_DB::get_post_variation($variation['v']);
        }

        if ($variation) {
            $post_title = $variation->post_title;
            $this->can_cache = false;
        }

        return $post_title;
    }

    /**
     * Override the "post" type permalink.
     */
    public function post_link($permalink, $post, $leavename)
    {
        if ($this->is_bot() || is_preview() || is_feed() || !in_array(get_post_type($post), array('post'))) {
            return $permalink;
        }

        $post = get_post($post);
        if (!$post) {
            return $permalink;
        }

        $variation = $this->get_post_variation($post->ID);
        if ($variation) {
            $this->can_cache = false;
            return add_query_arg($this->get_var_name(), $variation['v'], $permalink);
        }

        return $permalink;
    }

    /**
     * Override the "page" type permalink.
     */
    public function page_link($permalink, $post, $sample)
    {
        if ($this->is_bot() || is_preview() || is_feed() || !in_array(get_post_type($post), array('page'))) {
            return $permalink;
        }

        $post = get_post($post);
        if (!$post) {
            return $permalink;
        }

        $variation = $this->get_post_variation($post->ID);
        if ($variation) {
            $this->can_cache = false;
            return add_query_arg($this->get_var_name(), $variation['v'], $permalink);
        }

        return $permalink;
    }

    public function get_cookie_name()
    {
        return 'hvid';
    }

    public function get_var_name()
    {
        return 'hvid';
    }

	//sessions the variations
    public function get_post_variations()
    {
        if (null === $this->variations) {
            $cookie = $this->get_cookie_name();

            if (!isset($_SESSION[$cookie])) {
                return array();
            }

            $variations = $_SESSION[$cookie];

            // process our session, keep it lean and mean and clean
            foreach ($variations as $id => $data) {
                // validate data structure, who knows...
                if (!$data || !is_array($data) || !isset($data['v']) || !is_string($data['v']) || empty($data['v'])) {
                    unset($variations[$id]);
                    continue;
                }

                // expire old entries +24 hours old
                if (isset($data['t']) && $data['t']+86400 < time()) {
                    unset($variations[$id]);
                    continue;
                }
            }

            $this->variations = $variations;

            $_SESSION[$this->get_cookie_name()] = $this->variations;
        }

        return $this->variations;
    }
	//sets the post id to match the varitions
    public function set_post_variation($post_id, $variation, $select = 0)
    {
        $variations = $this->get_post_variations();

        $post = get_post($post_id);
        if ($post) {
            $variations[$post->ID] = array(
                'v' => $variation,
                't' => time(0),
                's' => $select
            );

            $this->variations = $variations;

            $_SESSION[$this->get_cookie_name()] = $this->variations;
        }
    }
	//returns the post variations 
    public function get_post_variation($post_id, $redirect = 0)
    {
        $variations = $this->get_post_variations();

        $post = get_post($post_id);
        if (!$post) {
            return false;
        }

        if (isset($variations[(int) $post->ID]) && $variations[(int) $post->ID]) {
            return $variations[(int) $post->ID];
        }
		
		//get the number of variations
        $count = Headlines_DB::get_num_post_variations($post->ID);
		
		
		//if number of variations is less than 0 then get the URL Key and match with Post ID.
        if ($count > 0) {
          $variation = Headlines_DB::get_bandit_variation($post->ID);
          if ($variation) {
              $this->set_post_variation($post->ID, $variation->url_key, $redirect);

              $post_guid = get_post_meta($post->ID, 'headlines_post_guid', true);
              $variation_guid = $variation->variation_guid;

              $this->script_queue[] = $this->generate_image_write($this->generate_image_url('headlines_visit', $post_guid, $variation_guid));
              $this->can_cache = false;

              return $this->variations[$post->ID];
          }
        }

        return false;
    }
	
	//check if its a user or bot
    public function check_post_variation()
    {
        if (!is_single() || $this->is_bot() || is_preview() || is_feed()) {
            return;
        }

        $post_guid = null;
        $variation_guid = null;
		
        $post = get_post();
        if (!$post || (get_post_type($post) != 'page' && get_post_type($post) != 'post')) {
            return;
        }

        if (isset($_GET[$this->get_var_name()])) {
            $variation = Headlines_DB::get_post_variation($_GET[$this->get_var_name()]);
            if ($variation) {
                $variation_guid = $variation->variation_guid;
                $post_guid = get_post_meta($post->ID, 'headlines_post_guid', true);
            }

            $variations = $this->get_post_variations();
            if (isset($variations[$post->ID])) {
                // if this post is in our session and its our first visit from a select
                if (isset($variations[$post->ID]['s']) && $variations[$post->ID]['s'] == 1) {
                    $this->script_queue[] = $this->generate_image_write($this->generate_image_url('headlines_visit', $post_guid, $variation_guid));
                    $this->can_cache = false;
                }
            } else {
                // not in our session, must have came direct so it's a viral hit
                $this->script_queue[] = $this->generate_image_write($this->generate_image_url('headlines_score', $post_guid, $variation_guid));
                $this->can_cache = false;
            }

            // this will update timestamp and set select = 0
            $this->set_post_variation($post->ID, $_GET[$this->get_var_name()]);

        } else {
            $variation = $this->get_post_variation($post->ID, 1);

            if ($variation) {
                $uri = add_query_arg($this->get_var_name(), $variation['v'], $_SERVER['REQUEST_URI']);
                wp_safe_redirect($uri);
                exit;
            }
        }
    }

	//conversion variables for later use
    public function query_vars($query_vars)
    {
        $query_vars[] = 'headlines_visit';
        $query_vars[] = 'headlines_score';

        return $query_vars;
    }
	
	//creates a unique id for each visit
    private function get_sign_verifier()
    {
        if (null == $this->sign_verifier)
        {
            $verifier = get_option(KS_HEADLINES_OPTION_SIGN_VERIFIER);

            if (!$verifier) {
                $verifier = md5(uniqid());

                update_option(KS_HEADLINES_OPTION_SIGN_VERIFIER, $verifier);
            }

            $this->sign_verifier = $verifier;
        }

        return $this->sign_verifier;
    }
	
    public function check_parse_request($wp)
    {
        $gif = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

        // log a visit and return a 1x1 GIF
        if (isset($wp->query_vars['headlines_visit'])) {

            if ($this->is_bot()) {
                header('Content-Type: image/gif');
                echo base64_decode($gif);
                exit;
            }

            $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
            $variation_id = isset($_GET['variation_id']) ? $_GET['variation_id'] : null;
            $passed_signature = isset($_GET['sig']) ? $_GET['sig'] : null;
            $ts = isset($_GET['ts']) ? $_GET['ts'] : 0;
            $age = time() - $ts;

            $verifier = $this->get_sign_verifier();
            $calculated_signature = md5('headlines_visit' . $ts . $verifier . $post_id . $variation_id);

            if ($post_id && $variation_id && $age <= 60 && $calculated_signature == $passed_signature) {
                try {
                    Headlines_DB::update_visit($post_id, $variation_id);
                } catch(Exception $e) {

                }
            }

            header('Content-Type: image/gif');
            echo base64_decode($gif);
            exit;
        }

        // log a score and return a 1x1 GIF
        if (isset($wp->query_vars['headlines_score'])) {

            if ($this->is_bot()) {
                header('Content-Type: image/gif');
                echo base64_decode($gif);
                exit;
            }

            $post_id = isset($_GET['post_id']) ? $_GET['post_id'] : null;
            $variation_id = isset($_GET['variation_id']) ? $_GET['variation_id'] : null;
            $passed_signature = isset($_GET['sig']) ? $_GET['sig'] : null;
            $ts = isset($_GET['ts']) ? $_GET['ts'] : 0;
            $age = time() - $ts;

            $verifier = $this->get_sign_verifier();
            $calculated_signature = md5('headlines_score' . $ts . $verifier . $post_id . $variation_id);

            if ($post_id && $variation_id && $age <= 60 && $calculated_signature == $passed_signature) {
                try {
                    Headlines_DB::update_score($post_id, $variation_id);
                } catch(Exception $e) {

                }
            }

            header('Content-Type: image/gif');
            echo base64_decode($gif);
            exit;
        }
    }
	//generates the conversion image
    private function generate_image_write($src)
    {
        return "document.write('<img src=\"{$src}\" alt=\"\" />');";
    }
	//generates the url
    private function generate_image_url($type, $post_guid, $variation_guid)
    {
        $ts = time();

        $verifier = $this->get_sign_verifier();
        $calculated_signature = md5($type . $ts . $verifier . $post_guid . $variation_guid);

        $site = get_bloginfo('wpurl');

        $data = array(
            $type => 1,
            'ts' => $ts,
            'post_id' => $post_guid,
            'variation_id' => $variation_guid,
            'sig' => $calculated_signature
        );

        return $site . '/index.php?' . http_build_query($data);
    }

    public function append_script_queue()
    {
        if (!empty($this->script_queue)) {
            echo '<script type="text/javascript">';
            echo implode("\r\n", $this->script_queue);
            echo '</script>';
        }
    }

    public function get_cache_key()
    {
        $keys = array();

        $keys[] = session_id();

        $variations = $this->get_post_variations();
        foreach ($variations as $variation) {
          $keys[] = $variation['v'];
        }
        sort($keys);
        $keys = implode(',', $keys);
        error_log('key is: ' . $keys);

        return $keys;
    }

    public function get_w3tc_cache_key($key)
    {
        $key = md5($key . $this->get_cache_key());

        return $key;
    }

    public function start_session()
    {
        if (!session_id()) {
            session_name(KS_HEADLINES_SESSION_COOKIE);
            session_set_cookie_params(24*3600*30);
            session_start();
        }
    }

    public function is_bot()
    {
        if (null === $this->bot_check_result) {
            if (!isset($_SERVER['HTTP_USER_AGENT'])) {
                $this->bot_check_result = false;
                return false;
            }

            $bots = array(
                'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
                'Googlebot/2.1 (+http://www.google.com/bot.html)',
                'Googlebot-News',
                'Googlebot-Image/1.0',
                'Googlebot-Video/1.0'
            );

            $this->bot_check_result = false;
            foreach ($bots as $bot) {
                if (strpos($_SERVER['HTTP_USER_AGENT'], $bot) !== false) {
                    $this->bot_check_result = true;
                    break;
                }
            }
        }

        return $this->bot_check_result;
    }
}
