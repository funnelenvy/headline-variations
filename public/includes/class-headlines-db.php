<?php

require_once 'class-base62.php';
require_once KS_HEADLINES_PLUGIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-headlines-rest.php';

class Headlines_DB
{
    const VERSION = 2;

    static protected $rest = null;

    public static function rest()
    {
        if (null == self::$rest)
        {
            self::$rest = new Headlines_REST();
        }

        return self::$rest;
    }
	//creates a new database for the headlines based on the db version
    public static function check_database_version()
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        $current_db_version = get_option(KS_HEADLINES_OPTION_DB_VERSION, 0);

        if (true) {
            self::install_table();
        }
    }
	//this is hidden on the class-headlines.php but it's mean to destroy the table if the plugin is deactivated
    public static function destroy()
    {
        global $wpdb;

        $table = self::get_tablename();

        $sql = "DROP TABLE `{$table}`";
        $wpdb->query($sql);

        delete_option(KS_HEADLINES_OPTION_DB_VERSION);
    }
	//this is the table create function
    private static function install_table()
    {
        $table = self::get_tablename();

        global $wpdb;

        // check for existance of column
        $table_existed = ($wpdb->query("SHOW TABLES LIKE '{$table}'") > 0);
        if ($table_existed) {
            $baseline_existed = ($wpdb->query("SELECT `is_baseline` FROM {$table}") !== false);
        }

        $sql = "CREATE TABLE `{$table}` (
  ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  post_id bigint(20) unsigned NOT NULL,
  variation_guid varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  status tinyint(1) NOT NULL DEFAULT '1',
  post_title text CHARACTER SET utf8 NOT NULL,
  is_primary tinyint(1) NOT NULL DEFAULT '0',
  is_baseline tinyint(1) NOT NULL DEFAULT '0',
  sort_order int(10) unsigned NOT NULL DEFAULT '1',
  url_key varchar(22) CHARACTER SET utf8 NOT NULL DEFAULT '',
  visit_count bigint(20) unsigned NOT NULL DEFAULT 0,
  viral_count bigint(20) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY  (ID),
  KEY variation_post (post_id),
  KEY variation_post_status (post_id,status),
  KEY variation_post_primary_status (post_id,is_primary,status),
  KEY variation_post_baseline (post_id,is_baseline),
  KEY variation_urlkey_status (url_key,status),
  KEY variation_guid (variation_guid),
  KEY variation_guid_post (variation_guid,post_id),
  KEY variation_guid_status (variation_guid,status)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";

        dbDelta($sql);

        // auto-update baseline value for oooold users
        if ($table_existed && $baseline_existed === false) {
            $wpdb->query("UPDATE {$table} SET `is_baseline` = `is_primary` WHERE `status` = '1'");
        }

        update_option(KS_HEADLINES_OPTION_DB_VERSION, self::VERSION);
    }
	//return the table name
    public static function get_tablename()
    {
        global $wpdb; //connects to database

        return $wpdb->prefix . 'headlines_variations'; //select and return the headlines variations table
    }
	
	//return the number of variations
    public static function get_num_post_variations($post_id, $include_deleted = false)
    {
        global $wpdb;

        $post = get_post($post_id); //returns post id
        $table = self::get_tablename(); //selects the table we globaled earlier

        if ($include_deleted) {
            $query = $wpdb->prepare("SELECT COUNT(*) FROM {$table} WHERE `post_id` = %d", $post->ID);
        } else {
            $query = $wpdb->prepare("SELECT COUNT(*) FROM {$table} WHERE `post_id` = %d AND `status` = 1", $post->ID);
        }

        return (int) $wpdb->get_var($query);
    }
    
	//return post variations by post id
    public static function get_post_variations($post_id, $output_type = OBJECT)
    {
        global $wpdb;

        $post = get_post($post_id);
        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE `post_id` = %d AND `status` = '1' ORDER BY sort_order ASC", $post->ID);

        return $wpdb->get_results($query, $output_type);
    }
	
	//return the primary variation (original headline)
    public static function get_primary_post_variation($post_id, $output_type = OBJECT)
    {
        global $wpdb;

        $post = get_post($post_id);
        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE `post_id` = %d AND `is_primary` = '1' AND `status` = '1'", $post->ID);

        $variation = $wpdb->get_row($query, $output_type);
        $variation = self::ensure_variation_urlkey($variation);

        return $variation;
    }
	//returns the baseline variation (original headline)
    public static function get_post_baseline_variation($post_id, $output_type = OBJECT)
    {
        global $wpdb;

        $post = get_post($post_id);
        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE post_id = %d AND `is_baseline` = '1'", $post->ID);

        $variation = $wpdb->get_row($query, $output_type);
        $variation = self::ensure_variation_urlkey($variation);

        return $variation;
    }

	//return url key (this tells the server which variation to load based on the url /?hvid=)
    public static function get_post_variation($urlkey, $output_type = OBJECT)
    {
        global $wpdb;

        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE url_key = '%s' AND `status` = '1'", $urlkey);

        $variation = $wpdb->get_row($query, $output_type);
        $variation = self::ensure_variation_urlkey($variation);

        return $variation;
    }
	//update the visit count based on the $url_key
    public static function update_visit($post_guid, $variation_guid)
    {
        global $wpdb;

        $table = self::get_tablename();

        $query = $wpdb->prepare("UPDATE {$table} SET visit_count = visit_count + 1 WHERE variation_guid = %s", $variation_guid);
        $wpdb->query($query);

        Headlines_REST::update_visit($post_guid, $variation_guid);
    }
	
	//update the score (Viral_Count)
    public static function update_score($post_guid, $variation_guid)
    {
        global $wpdb;

        $table = self::get_tablename();

        $query = $wpdb->prepare("UPDATE {$table} SET viral_count = viral_count + 1 WHERE variation_guid = %s", $variation_guid);
        $wpdb->query($query);

        Headlines_REST::update_score($post_guid, $variation_guid);
    }
	
	//returns the variation based on the post meta and if true set the url key 
    public static function get_bandit_variation($post_id, $output_type = OBJECT)
    {
    	  global $wpdb;

        $table = self::get_tablename();
        $post = get_post($post_id);
        $post_guid = get_post_meta($post->ID, 'headlines_post_guid', true);

        if ($post_guid) {
            try {
                $ret = Headlines_REST::select_variation($post_guid);
                if (is_array($ret)) {
                    $query = $wpdb->prepare("SELECT * FROM {$table} WHERE variation_guid = '%s' AND post_id = '%d'", $ret[0], $post->ID);
                    $variation = $wpdb->get_row($query, $output_type);

                    $variation = self::ensure_variation_urlkey($variation);

                    if ($variation) {
                      return $variation;
                    }
                }
            }
            catch(Exception $e) {
                //var_dump($e);die;
            }
        }

        return self::get_random_variation($post_id, OBJECT);
    }

	//returns a random variation to display
    public static function get_random_variation($post_id, $output_type = OBJECT)
    {
        $variations = self::get_post_variations($post_id, $output_type);

        if (empty($variations)) {
            return false;
        }

        $n = rand(0, count($variations) - 1);

        $variation = $variations[$n];
        $variation = self::ensure_variation_urlkey($variation);

        return $variation;
    }
	
	//inserts the posts titles variations
    public static function add_post_variation($post_id, $title, $sort_order = 1, $is_primary = false)
    {
        global $wpdb;

        $post = get_post($post_id);
        $table = self::get_tablename();

        $baseline = self::get_post_baseline_variation($post->ID, OBJECT);

        $wpdb->insert($table,
            array(
                'post_id' => $post->ID,
                'post_title' => $title,
                'sort_order' => $sort_order,
                'is_primary' => $is_primary,
                'is_baseline' => ($is_primary && $baseline == null),
                'url_key' => self::generate_title_urlkey($title)
            ),
            array(
                '%d',
                '%s',
                '%d',
                '%d',
                '%d',
                '%s'
            )
        );

        $id = $wpdb->insert_id;

        self::upload_post_variation($id);

        return $id;
    }
	//updates the post title variations
    public static function update_post_variation($id, $title, $sort_order = 1, $is_primary = false)
    {
        global $wpdb;

        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE ID = %d", $id);
        $existing = $wpdb->get_row($query, OBJECT);
        $baseline = self::get_post_baseline_variation($existing->post_id, OBJECT);

        // update existing record if there is a minor change or it's the primary title
        $difference = levenshtein($existing->post_title, $title);
        if ($difference <= 10 || ($is_primary && $existing->visit_count < 10)) {
            $wpdb->update($table,
                array(
                    'post_title' => $title,
                    'sort_order' => $sort_order,
                    'is_primary' => $is_primary,
                    'is_baseline' => ($is_primary && ($baseline == null || $baseline->ID == $id) ? true : false)
                ),
                array(
                    'ID' => $id
                ),
                array('%s','%d','%d','%d'),
                array('%d')
            );

            self::upload_post_variation($id);

            return $id;
        } else {
            return self::add_post_variation($existing->post_id, $title, $sort_order, $is_primary);
        }
    }
	
	//if the get_meta_post was displayed update the visit/goal count
    public static function update_variation_stats($variation_guid, $visits, $goals)
    {
        if (!$variation_guid) {
            return;
        }

        global $wpdb;
        $table = self::get_tablename();

        $wpdb->update($table,
            array(
                'visit_count' => $visits,
                'viral_count' => $goals
            ),
            array(
                'variation_guid' => $variation_guid
            ),
            array('%d','%d'),
            array('%s')
        );
    }

	//deletes the variation from the table
    public static function delete_post_variations($post, $ids)
    {
        global $wpdb;

        $post = get_post($post);
        $table = self::get_tablename();

        $query = sprintf("UPDATE $table SET `status` = '0' WHERE ID NOT IN (%s) AND post_id = %d", implode(',', $ids), $post->ID);

        $wpdb->query($query);

        // fetch list of local variations
        $variations = self::get_post_variations($post, OBJECT);

        $guids = array();
        foreach ($variations as $variation) {
            $guids[] = $variation->variation_guid;
        }

        try {
            $post_guid = get_post_meta($post->ID, 'headlines_post_guid', true);
            if (!$post_guid) throw new Exception('No post GUID.');

            // fetch list of remote variations
            $variations = self::rest()->get_variations($post_guid);
            $delete_guids = array();
            foreach ($variations as $variation) {

                // remote variation no longer exists locally
                $valid = in_array($variation['id'], $guids);
                if (!$valid) {
                    $delete_guids[] = $variation['id'];
                }
            }

            // delete orphan variations from server
            foreach ($delete_guids as $guid) {
                self::rest()->delete_variation($guid);
            }
        } catch(Exception $e) {
            //var_dump($e);die;
        }
    }

	//updates original post
    public static function upload_post($post)
    {
        $post = get_post($post);

        $data = array(
            'url' => get_permalink($post)
        );

        // TODO - update
        try {
            $ret = self::rest()->add_post($data);

            update_post_meta($post->ID, 'headlines_post_guid', $ret['id']);

            return $ret['id'];
        } catch(Exception $e) {
            //var_dump($e->getMessage());die;
        }
    }
	
	//update/add variations in the table
    public static function upload_post_variation($id)
    {
        global $wpdb;

        $table = self::get_tablename();

        $query = $wpdb->prepare("SELECT * FROM {$table} WHERE ID = %d", $id);

        $variation = $wpdb->get_row($query, OBJECT);

        $post_guid = get_post_meta($variation->post_id, 'headlines_post_guid', true);
        if (!$post_guid) {
            $post_guid = self::upload_post($variation->post_id);
        }

        $data = array(
            'title' => $variation->post_title,
            'isBaseline' => $variation->is_baseline,
            'sortOrder' => $variation->sort_order,
            'postId' => $post_guid,
        );

        try {
            if ($variation->variation_guid) {
                $data['id'] = $variation->variation_guid;
                $ret = self::rest()->update_variation($data);
            } else {
                $ret = self::rest()->add_variation($data);
            }

            $wpdb->update($table,
                array('variation_guid' => $ret['id']),
                array('ID' => $id),
                array('%s'),
                array('%d')
            );
        } catch(Pest_NotFound $e) {
            // invalid id - out of sync
            $wpdb->update($table,
                array('variation_guid' => ''),
                array('ID' => $id),
                array('%s'),
                array('%d')
            );

            self::upload_post_variation($id);
        } catch(Pest_BadRequest $e) {
            // invalid postId - out of sync
            delete_post_meta($variation->post_id, 'headlines_post_guid');

            self::upload_post_variation($id);
        } catch(Exception $e) {
            //var_dump($e);die;
        }
    }
	
	//generate a hash url_key
    static public function generate_title_urlkey($title)
    {
        $hash = (float) sprintf('%u', crc32($title));
        $hash += rand();

        return Base62::convert($hash);
    }
	
	//assigns the url_key to the variation
    static public function ensure_variation_urlkey($variation)
    {
        if ($variation) {
            if (is_object($variation) && isset($variation->ID) && isset($variation->post_title) && isset($variation->url_key)) {
                if (!trim($variation->url_key)) {
                    $key = self::generate_title_urlkey($variation->post_title);

                    if ($key) {
                        global $wpdb;

                        $table = self::get_tablename();
                        $wpdb->update($table,
                            array(
                                'url_key' => $key
                            ),
                            array(
                                'ID' => $variation->ID
                            ),
                            array('%s'),
                            array('%d')
                        );

                        $variation->url_key = $key;
                    }
                }
            }
        }

        return $variation;
    }
}
