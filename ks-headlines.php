<?php
/**
 * WordPress Plugin Boilerplate.
 *
 * @wordpress-plugin
 * Plugin Name:       KingSumo Headlines
 * Plugin URI:        http://kingsumo.com/apps/headlines
 * Description:       Simple headline testing for WordPress.
 * Version:           1.0.5
 * Author:            KingSumo
 * Author URI:        http://kingsumo.com
 * Text Domain:       plugin-name-locale
 * Domain Path:       /languages
 */

//checks to make sure wp-includes folder is there
if (!defined('WPINC')) {
    die;
}


// ensure it matches EDD backend
define('KS_HEADLINES_EDD_NAME', 'KingSumo Headlines');
define('KS_HEADLINES_EDD_VERSION', '1.0.5');
define('KS_HEADLINES_EDD_URL', 'http://kingsumo.com');
define('KS_HEADLINES_EDD_AUTHOR', 'KingSumo');

//defines a constant value, in this case it's just letting the plugin know to include these
define('KS_HEADLINES_PLUGIN_DIR', dirname(__FILE__));
define('KS_HEADLINES_PLUGIN_INCLUDES_DIR', KS_HEADLINES_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'includes');

define('KS_HEADLINES_PLUGIN_PUBLIC_DIR', KS_HEADLINES_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'public');
define('KS_HEADLINES_PLUGIN_PUBLIC_INCLUDES_DIR', KS_HEADLINES_PLUGIN_PUBLIC_DIR . DIRECTORY_SEPARATOR . 'includes');
define('KS_HEADLINES_PLUGIN_PUBLIC_VIEWS_DIR', KS_HEADLINES_PLUGIN_PUBLIC_DIR . DIRECTORY_SEPARATOR . 'views');

define('KS_HEADLINES_PLUGIN_ADMIN_DIR', KS_HEADLINES_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'admin');
define('KS_HEADLINES_PLUGIN_ADMIN_INCLUDES_DIR', KS_HEADLINES_PLUGIN_ADMIN_DIR . DIRECTORY_SEPARATOR . 'includes');
define('KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR', KS_HEADLINES_PLUGIN_ADMIN_DIR . DIRECTORY_SEPARATOR . 'views');

define('KS_HEADLINES_OPTION_LICENSE_KEY', 'ks_headlines_license_key');
define('KS_HEADLINES_OPTION_LICENSE_STATUS', 'ks_headlines_license_status');
define('KS_HEADLINES_OPTION_SIGN_VERIFIER', 'ks_headlines_sign_verifier');
define('KS_HEADLINES_OPTION_SITE_ID', 'ks_headlines_site_id');
define('KS_HEADLINES_OPTION_SITE_TOKEN', 'ks_headlines_site_token');
define('KS_HEADLINES_OPTION_DB_VERSION', 'ks_headlines_db_version');
define('KS_HEADLINES_OPTION_REST_SERVER_URL', 'ks_headlines_rest_url');

define('KS_HEADLINES_SESSION_COOKIE', 'kshlsid');

require_once(KS_HEADLINES_PLUGIN_PUBLIC_DIR . DIRECTORY_SEPARATOR . 'class-headlines.php');

//registration hooks
register_activation_hook(__FILE__, array('Headlines', 'activate'));
register_deactivation_hook(__FILE__, array('Headlines', 'deactivate'));

//gets the necessary headlines code once the plugin was finished activating
add_action('plugins_loaded', array('Headlines', 'get_instance'));

//checks if you're an admin, if you are, it gets the admin settings
if (is_admin()) {
    require_once(KS_HEADLINES_PLUGIN_ADMIN_DIR . DIRECTORY_SEPARATOR . 'class-headlines-admin.php');
    add_action('plugins_loaded', array('Headlines_Admin', 'get_instance'));
}
