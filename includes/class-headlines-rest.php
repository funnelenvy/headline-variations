<?php

require_once KS_HEADLINES_PLUGIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'pest/Pest.php';
require_once KS_HEADLINES_PLUGIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'pest/PestJSON.php';
//handles activation
class Headlines_REST
{
    static protected $pest = null;

    public static function pest()
    {
        if (null == self::$pest)
        {
            self::$pest = new PestJSON(get_option(KS_HEADLINES_OPTION_REST_SERVER_URL, 'http://headlines.kingsumo.com'));

            $site_id = trim(get_option(KS_HEADLINES_OPTION_SITE_ID));
            $site_token = trim(get_option(KS_HEADLINES_OPTION_SITE_TOKEN));
            if (!$site_id || !$site_token) {
                try {
                    $ret = self::create_site(array(
                        'url' => get_bloginfo('url'),
                        'licenseKey' => get_option(KS_HEADLINES_OPTION_LICENSE_KEY)
                    ));

                    $site_id = $ret['id'];
                    update_option(KS_HEADLINES_OPTION_SITE_ID, $site_id);

                    $site_token = $ret['token'];
                    update_option(KS_HEADLINES_OPTION_SITE_TOKEN, $site_token);

                } catch(Exception $e) {
                    //var_dump($e);die;
                }
            }

            self::$pest->setupAuth($site_id, $site_token);
        }

        return self::$pest;
    }

    public static function create_site($data)
    {
        return self::pest()->post('/site', $data);
    }

    public static function add_post($data)
    {
        return self::pest()->post('/post', $data);
    }

    public static function add_variation($data)
    {
        return self::pest()->post('/variation', $data);
    }

    public static function update_variation($data)
    {
        return self::pest()->put('/variation', $data);
    }

    public static function delete_variation($id)
    {
        return self::pest()->delete('/variation/' . $id);
    }

    public static function get_variations($post_id)
    {
        return self::pest()->get('/variation?' . http_build_query(array('postId' => $post_id)));
    }

    public static function update_visit($post_id, $variation_id)
    {
        return self::pest()->put('/brain/visit/' . $post_id . '/' . $variation_id, array('ip' => $_SERVER['REMOTE_ADDR']));
    }

    public static function update_score($post_id, $variation_id)
    {
        return self::pest()->put('/brain/score/' . $post_id . '/' . $variation_id, array('ip' => $_SERVER['REMOTE_ADDR']));
    }

    public static function select_variation($post_id)
    {
        return self::pest()->get('/brain/select/' . $post_id . '?ip=' . $_SERVER['REMOTE_ADDR']);
    }

    public static function post_stats_timeframe($post_id)
    {
        return self::pest()->get('/stats/post/timeframe/' . $post_id);
    }

    public static function total_post_stats($post_id)
    {
        return self::pest()->get(sprintf('/stats/post/total/%s', $post_id));
    }

    public static function monthly_post_stats($post_id, $month)
    {
        return self::pest()->get(sprintf('/stats/post/monthly/%s/%s', $post_id, $month));
    }
}