<?php

class Headlines_Session
{
    private $session_id = null;

    /**
     * Starts session if there isn't any. Instance of this class.
     */
    protected static $instance = null;

    public function __construct()
    {
        if (isset($_COOKIE[KS_HEADLINES_SESSION_COOKIE])) {
            $this->session_id = $_COOKIE[KS_HEADLINES_SESSION_COOKIE];
        }

        if (!$this->session_id) {
            $this->session_id = md5(uniqid());
        }

        $this->expires = time() + 24*60*60;

        $this->read_session_data();

        setcookie(KS_HEADLINES_SESSION_COOKIE, $this->session_id, $this->expires, '/');
    }

    /**
     * Returns an instance of this class.
     *
     * @return  object    A single instance of this class.
     */
    public static function get_instance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private function read_session_data()
    {

    }

    private function write_session_data()
    {

    }
}