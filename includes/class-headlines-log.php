<?php

class Headlines_Log
{
    public static function check_database_version()
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        do {
            $current_db_version = get_option('headlines_db_log_version', 0);

            switch($current_db_version) {
                case 0: self::install_version_1(); break;
            }
        } while ($current_db_version < 1);
    }

    public static function destroy()
    {
        global $wpdb;

        $table = self::get_tablename();

        $sql = "DROP TABLE `{$table}`";
        $wpdb->query($sql);

        delete_option('headlines_db_log_version');
    }

    private static function install_version_1()
    {
        $table = self::get_tablename();

        $sql = "CREATE TABLE `{$table}` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) NULL,
  `line_number` int(10) unsigned NULL,
  `function_name` varchar(100) NULL
  PRIMARY KEY (`ID`)
);";

        dbDelta($sql);

        update_option('headlines_db_log_version', 1);
    }

    private static function get_tablename()
    {
        global $wpdb;

        return $wpdb->prefix . 'headlines_log';
    }

    public static function log($message, $file = null, $line = null, $function = null)
    {
        global $wpdb;

        $data = array();
        $formats = array();

        if ($file) {
            $data['file_name'] = basename($file);
            $formats[] = '%s';
        }

        if ($line) {
            $data['line_number'] = $line;
            $formats[] = '%d';
        }

        if ($function) {
            $data['function_name'] = $function;
            $formats[] = '%s';
        }

//        $wpdb->insert();
    }
}