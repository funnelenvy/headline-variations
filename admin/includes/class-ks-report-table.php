<?php

class KS_Report_List_Table extends WP_List_Table
{
    private $post_id = null;

    private $post_guid = null;

    private $months = array();

    private $month = null;

    public function __construct($args = array())
    {
        $this->post_id = $args['post_id'];
        $this->post_guid = $args['post_guid'];
        $this->months = $args['months'];
        $this->month = $args['month'];

        parent::__construct(array(
            'plural' => 'report',
            'screen' => null
        ));
    }

    public function single_row($item) {
        static $row_class = '';
        static $current_date = '';

        $row_class = ($row_class == '' ? ' alternate' : '');

        if (isset($item['date'])) {
            $extra_class = '';
            if ($current_date == 'total stat') {
                $extra_class = 'start';
            }

            $current_date = $item['date'];

            echo '<tr class="header date-' . $current_date . ' ' . $extra_class . '"><td colspan="5" class="header"><strong>';
            echo $this->column_default($item, 'date');
            echo '</strong></td></tr>';

            $current_date .= ' stat';
        }

        echo '<tr class="' . $row_class . ' date-' . $current_date . '">';
        $this->single_row_columns($item);
        echo '</tr>';
    }

		public function single_row_columns($item)
    {
        list($columns, $hidden) = $this->get_column_info();

        foreach ($columns as $column_name => $column_display_name) {
            $class = "class='$column_name column-$column_name'";

            $style = '';
            if (in_array($column_name, $hidden))
                $style = ' style="display:none;"';

            $rowspan = '';
            if ($column_name == 'date' && isset($item['date'])) {
                $rowspan = ' rowspan="' . $item['rowspan'] . '"';
            } else if ($column_name == 'date' && !isset($item['date'])) {
                continue;
            }

            $attributes = "$class$style$rowspan";

            if ( 'cb' == $column_name ) {
                echo '<th scope="row" class="check-column">';
                echo $this->column_cb( $item );
                echo '</th>';
            }
            elseif ( method_exists( $this, 'column_' . $column_name ) ) {
                echo "<td $attributes>";
                echo call_user_func( array( $this, 'column_' . $column_name ), $item );
                echo "</td>";
            }
            else {
                echo "<td $attributes>";
                echo $this->column_default( $item, $column_name );
                echo "</td>";
            }
        }
    }

    public function no_items()
    {
        if ($this->post_guid) {
            _e('No statistics found.');
        } else {
            _e('Select a post to filter on.');
        }
    }

    public function prepare_items()
    {
        $items = array();
        if ($this->post_guid) {
            try {
                $data = Headlines_REST::total_post_stats($this->post_guid);
                $group = array('date' => 'total');

                foreach ($data as $id => $info) {
                    Headlines_DB::update_variation_stats($id, (int) $info['visits'], (int) $info['goals']);

                    $items[] = array_merge($group, $info);
                    $group = array();
                }

                $data = Headlines_REST::monthly_post_stats($this->post_guid, $this->month);

                foreach ($data as $date => $row) {
                    $group = array(
                        'date' => $date,
                        'rowspan' => count($row)
                    );
                    foreach ($row as $id => $info) {
                        $items[] = array_merge($group, $info);
                        $group = array();
                    }
                }
            }
            catch(Exception $e) {
              //var_dump($e);die;
            }
        }

        $this->items = $items;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
    }

    public function column_default($item, $column_name)
    {
        switch($column_name) {
            case 'date':
                if ($item[$column_name] == 'total') {
                    return 'Totals';
                }

                list($y,$m,$d) = explode('-', $item[$column_name]);

                $ts = mktime(0, 0, 0, $m, $d, $y);

                return date_i18n(get_option('date_format'), $ts);

            case 'goals':
            case 'visits':
                return number_format($item[$column_name]);

            case 'conversion_delta':
                if (!isset($item[$column_name])) {
                    return;
                }

                $delta = $item[$column_name];
                if ($delta > 0) {
                    return '<span class="positive">+' . number_format($item[$column_name]*100, 2) . '%</span>';
                } else if ($delta < 0) {
                    return '<span class="negative">' . number_format($item[$column_name]*100, 2) . '%</span>';
                } else {
                    return number_format($item[$column_name]*100, 2) . '%';
                }
                break;

            case 'conversion_rate':
                return number_format($item[$column_name]*100, 2) . '%';

            default:
                return $item[$column_name];
        }
    }

    public function display_tablenav($which) {
        if ($which == 'top') {
            global $wp_locale;

            $filter = '';
            if (!empty($this->months)) {
                $filter = '<select name="month">';
                foreach ($this->months as $date) {
                    list($y,$m,$d) = explode('-', $date);

                    $ts = mktime(0, 0, 0, $m, $d, $y);
                    $selected = $date == $this->month ? ' selected' : '';
                    $filter .= sprintf('<option value="%1$s"%4$s>%2$s %3$d</option>', $date, $wp_locale->get_month($m), $y, $selected);
                }
                $filter .= '</select>';
            }

            echo <<<EOF
<div class="tablenav top">
  <div class="alignleft">
    <form method="get" action="admin.php">
      <input type="hidden" name="page" value="ks-headlines" />
      <input type="hidden" id="ks-report-post" name="post_id" value="{$this->post_id}" />
      {$filter}

      <input type="submit" name="" class="button" value="Filter">
    </form>
  </div>
</div>
EOF;
        }
    }

    public function Xget_views()
    {
        $views = array();

        $views['all'] = '<a href="#">All <span class="count">(5)</span></a>';
        $views['posts'] = '<a href="#">Posts <span class="count">(5)</span></a>';
        $views['titles'] = '<a href="#">Titles <span class="count">(5)</span></a>';

        return $views;
    }

    public function get_columns()
    {
        $columns = array();
        $columns['title'] = 'Title';
        $columns['visits'] = 'Views';
        $columns['goals'] = 'Inbound Traffic';
        $columns['conversion_rate'] = 'Inbound / Views';
        $columns['conversion_delta'] = 'Improvement';

        return $columns;
    }

    public function get_sortable_columns()
    {
        return array(
            'date'
        );
    }
}