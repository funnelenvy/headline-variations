/*******************************
	OOPS Version 1.1.2

	A very simple "Object Oriented Programming Structured" class system for JavaScript

	The MIT License (MIT)

	Copyright (c) 2013 Greg McLeod <cleod9{at}gmail.com>

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
*******************************/
jQuery(document).ready(function($) {

var OOPS=function(){var e=function(){};e._ID_=0;e.extend=function(t){var n;var r=["_statics_","_constructor_","_super_"];var i=typeof t._constructor_=="function"?t._constructor_:function(){};i.prototype=typeof t=="object"?t:{};for(n in this.prototype)if(!i.prototype.hasOwnProperty(n))i.prototype[n]=this.prototype[n];if(i.prototype._statics_){for(n in i.prototype._statics_)i[n]=i.prototype._statics_[n];delete i.prototype._statics_}i.extend=this.extend;i.prototype._super_=i._super_=this!=e?this:null;i.prototype._ID_=i._ID_=++e._ID_;return i};e.typeMatch=function(e,t){return e._ID_==t._ID_};e.descendantOf=function(t,n){if(t._super_){if(t._super_._ID_==n._ID_)return true;else return e.descendantOf(t._super_,n)}return false};return e}()

var TitleEditor = OOPS.extend({
  install: function() {

    var $title = $('#title');
    var $row = $('<div class="headline-title-row"></div>');
    var $conv = $('<span class="variation-conversion"></span>');
    var $wrap = $('<div class="title-wrap original-title"></div>');
    var $pos = $('<span class="variation-position"></span>');

    $row.append($pos);
    $row.append($conv);
    $row.append($wrap);

    $title.before($row);
    $wrap.append($title);
    $title.before($('#titlewrap label'));

    this.$addVariationEl = $('<a href="#" class="button add-variation">Add Another Title</a>').insertAfter('#titlewrap');
    this.$addVariationEl.click($.proxy(this.addVariationRow, this));

    this.load();
  },

  load: function() {
    $.get(
      ajaxurl,
      {
        'action': 'load_post_variations',
        'post_id': $('#post_ID').val()
      },
      $.proxy(function(response) {
        var i;

        for (i = 0; i < response.length; i++) {
          var row;
          var cls = '';
          var prefix = '';

          if (i == 0) {
            row = $('div.headline-title-row').eq(0);

            $('#post_title').val(response[i].post_title);
          } else {
            row = this.addVariationRow();

            row.find('input[type="hidden"]').val(response[i].ID);
            row.find('input[type="text"]').val(response[i].post_title);
          }

          if (response[i].conversion_delta > 0) {
            cls = 'positive'
            prefix = '+';
          } else if (response[i].conversion_delta < 0) {
            cls = 'negative';
          }

          row.find('span.variation-conversion').text(prefix + response[i].conversion_delta.toFixed(1) + '%').addClass(cls);
        }

        this.updateVariationRowPositions();
      }, this)
    );
  },

  updateVariationRowPositions: function() {
    var pos = 0;

    $('#titlediv .headline-title-row').each(function() {
      $(this).find('.variation-position').html(pos+1);
      if (pos > 0) {
        $(this).find('input[type="hidden"]').attr('name', 'post_title_variation[' + pos + '][id]');
        $(this).find('input[type="text"]').attr('name', 'post_title_variation[' + pos + '][title]');
      }
      pos++;
    });
  },

  addVariationRow: function(e) {
    if (e) {
      e.preventDefault();
    }

    var $row = $('<div class="headline-title-row"></div>');
    var $pos = $('<span class="variation-position"></span>');
    var $conv = $('<span class="variation-conversion">0.0%</span>');
    var $remove = $('<a href="#" class="remove-variation button button-small" tabindex="-1">Remove</a>');
    var $wrap = $('<div class="title-wrap"></div>');
    var $id = $('<input type="hidden" name="post_title_variation[][id]" />');
    var $input = $('<input type="text" name="post_title_variation[][title]" size="30" autocomplete="off" />');

    $row.append($conv);
    $row.append($remove);
    $row.append($pos);
    $row.append($wrap);
    $row.append($id);
    $wrap.append($input);

    $('#titlewrap').append($row);

    this.updateVariationRowPositions();

    $remove.click($.proxy(this.removeVariationRow, this));

    $input.focus();

    return $row;
  },

  removeVariationRow: function(e) {
    if (e) {
      e.preventDefault();
    }

    var $row = $(e.target).closest('div.headline-title-row');
    $row.remove();
    this.updateVariationRowPositions();
  }
});

  var editor = new TitleEditor();
  editor.install();

});