<!-- This just checks the activation key and allows you to deactivate-->
<?php if (get_option(KS_HEADLINES_OPTION_LICENSE_STATUS) == 'valid'): ?>

<button class="button ks-headlines-deactivate">Deactivate</button>
<div style="font-size: 85%">
<span style="color:green">Your license key is currently active</span>
</div>

<?php else: ?>

<button class="button ks-headlines-activate">Activate</button>
<div style="font-size: 85%">
<span style="color:red">Your license key is currently inactive</span>
</div>

<?php endif ?>