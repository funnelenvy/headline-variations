<div class="wrap">
  <h2>KingSumo Headlines Statistics</h2>

  <?php echo $list_table->display() ?>

</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#ks-report-post').select2({
    width: '300px',
    ajax: {
      url: ajaxurl,
      data: function(term, page) {
        return {
          q: term,
          page: page,
          page_limit: 10,
          action: 'ks_autocomplete_posts'
        }
      },
      results: function(data, page) {
        var more = (page * 10) < data.total;

        return { results: data.posts, more: more };
      }
    }
    <?php if ($post): ?>
    , initSelection: function(element, cb) {
      cb({id: <?php echo $post->ID ?>, text: '<?php echo esc_js($post->post_title) ?>'});
    }
    <?php endif ?>
  });

  $('#ks-report-variation').select2({
    width: '200px',
    placeholder: 'Show all variations',
    allowClear: true
  });
});
</script>