<!-- Settings page -->
<div class="wrap">
  <h2><?php _e('KingSumo Headlines'); ?></h2>
  <form method="post" action="options.php">
    <?php settings_fields('headlines_options'); ?>
    <?php do_settings_sections('headlines-options') ?>
    <?php submit_button(); ?>
  </form>
</div>
<script type="text/javascript">
jQuery(document).ready(function($) {

  $('input[name="ks_headlines_license_key"]').on('keyup', function() {
    $('#ks-license-container').hide();
  });

  $('#ks-license-container').on('click', '.ks-headlines-activate', function(e) {
    e.preventDefault();
    e.stopPropagation();

    $('#ks-license-container button').prop('disabled', 'disabled');

    $.post(
      ajaxurl,
      {
        action: 'ks_activate_license'
      },
      function(response) {
        $('#ks-license-container').html(response);
        $('input[name="ks_headlines_license_key"]').prop('readonly', 'readonly');
      }
    );
  });

  $('#ks-license-container').on('click', '.ks-headlines-deactivate', function(e) {
    e.preventDefault();
    e.stopPropagation();

    if (!confirm('Are you sure you want to deactivate your license key?')) {
      return;
    }

    $('#ks-license-container button').prop('disabled', 'disabled');

    $.post(
      ajaxurl,
      {
        action: 'ks_deactivate_license'
      },
      function(response) {
        $('#ks-license-container').html(response);
        $('input[name="ks_headlines_license_key"]').prop('readonly', null);
      }
    );
  });

});
</script>