<?php

require_once KS_HEADLINES_PLUGIN_PUBLIC_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-headlines-db.php';
require_once(ABSPATH . '/wp-admin/includes/template.php');

if (!class_exists('EDD_SL_Plugin_Updater')) {
    require_once KS_HEADLINES_PLUGIN_ADMIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'EDD_SL_Plugin_Updater.php';
}

/**
 * @package     Headlines_Admin
 */
class Headlines_Admin
{
    /**
     * Instance of this class.
     */
    protected static $instance = null;

    private function __construct()
    {
        $plugin = Headlines::get_instance();
        $this->plugin_slug = $plugin->get_plugin_slug();

        add_action('admin_init', array($this, 'admin_init'));
        add_action('admin_menu', array($this, 'admin_menu'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));

        add_action('admin_notices', array($this, 'admin_notices'));

        add_action('wp_ajax_load_post_variations', array($this, 'load_post_variations'));
        add_action('wp_ajax_ks_autocomplete_posts', array($this, 'autocomplete_posts'));
        add_action('save_post', array($this, 'save_post_variations'));
    }

    /**
     * Returns an instance of this class.
     *
     * @return  object    A single instance of this class.
     */
    public static function get_instance()
    {
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function admin_init()
    {
        add_action('wp_ajax_ks_activate_license', array($this, 'ajax_activate_license'));
        add_action('wp_ajax_ks_deactivate_license', array($this, 'ajax_deactivate_license'));

        $this->updater = new EDD_SL_Plugin_Updater(
          KS_HEADLINES_EDD_URL,
          KS_HEADLINES_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'ks-headlines.php',
          array(
              'version' => KS_HEADLINES_EDD_VERSION,
              'item_name' => KS_HEADLINES_EDD_NAME,
              'author' => KS_HEADLINES_EDD_AUTHOR,
              'license' => get_option(KS_HEADLINES_OPTION_LICENSE_KEY)
          )
        );

        register_setting('headlines_options', KS_HEADLINES_OPTION_LICENSE_KEY, array($this, 'sanitize_license'));
        add_settings_section('headlines_settings_license', 'General Settings', null, 'headlines-options');
        add_settings_field(KS_HEADLINES_OPTION_LICENSE_KEY, 'License Key', array($this, 'input_license_key'), 'headlines-options', 'headlines_settings_license');
    }

    public function input_license_key()
    {
        echo sprintf('<input type="text" class="regular-text" name="ks_headlines_license_key" value="%s" />', get_option(KS_HEADLINES_OPTION_LICENSE_KEY));
        echo '<span id="ks-license-container">';
        include KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR . DIRECTORY_SEPARATOR . 'license_status.php';
        echo '</span>';
    }

    public function admin_menu()
    {
        add_options_page('KingSumo Headlines', 'KingSumo Headlines', 'manage_options', 'headlines-options', array($this, 'settings_page'));
        add_menu_page('KingSumo Headlines', 'KingSumo Headlines', 'manage_options', 'ks-headlines', array($this, 'headlines_page'), plugin_dir_url(__FILE__) . '/assets/images/headlines-icon-wp-20.png');
    }

    public function headlines_page()
    {
        require_once KS_HEADLINES_PLUGIN_ADMIN_INCLUDES_DIR . DIRECTORY_SEPARATOR . 'class-ks-report-table.php';

        $post = null;
        $post_guid = null;
        $post_id = isset($_REQUEST['post_id']) ? (int) $_REQUEST['post_id'] : null;
        $month = isset($_REQUEST['month']) ? $_REQUEST['month'] : null;

        if ($post_id) {
          $post = get_post($post_id);
          if ($post) {
              $post_guid = get_post_meta($post_id, 'headlines_post_guid', true);
          }
        }

        $months = array();

        try {
            global $wp_locale;

            $data = Headlines_REST::post_stats_timeframe($post_guid);

            $months = $data;

            if (!$month && !empty($months)) {
                $month = $months[0];
            }
        } catch(Exception $e) {

        }

        $list_table = new KS_Report_List_Table(array(
            'post_id' => $post_id,
            'post_guid' => $post_guid,
            'month' => $month,
            'months' => $months
        ));
        $list_table->prepare_items();
        require_once KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR . DIRECTORY_SEPARATOR . 'reports.php';
    }

    public function settings_page()
    {
        require_once KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR . DIRECTORY_SEPARATOR . 'settings.php';
    }

    public function ajax_activate_license()
    {
        $this->activate_license(get_option(KS_HEADLINES_OPTION_LICENSE_KEY));

        include KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR . DIRECTORY_SEPARATOR . 'license_status.php';
        exit;
    }

    public function ajax_deactivate_license()
    {
        $this->activate_license(get_option(KS_HEADLINES_OPTION_LICENSE_KEY), 'deactivate_license');

        include KS_HEADLINES_PLUGIN_ADMIN_VIEWS_DIR . DIRECTORY_SEPARATOR . 'license_status.php';
        exit;
    }

    public function activate_license($license, $action = 'activate_license')
    {
        if ($license) {
            $api_params = array(
                'edd_action'=> $action,
                'license' 	=> $license,
                'item_name' => urlencode(KS_HEADLINES_EDD_NAME)
            );

            $response = wp_remote_get(add_query_arg($api_params, KS_HEADLINES_EDD_URL), array('timeout' => 15, 'sslverify' => false));

            if (is_wp_error($response)) {
                return false;
            }

            $license_data = json_decode(wp_remote_retrieve_body($response));
            update_option(KS_HEADLINES_OPTION_LICENSE_STATUS, $license_data->license);
        }
    }

    public function sanitize_license($new)
    {
        $old = get_option(KS_HEADLINES_OPTION_LICENSE_KEY);
        if ($old != $new) {
            delete_option(KS_HEADLINES_OPTION_LICENSE_STATUS);

            if (trim($new)) {
                $this->activate_license($new);
            }
        }

        return $new;
    }

    /**
     * Register and enqueue the admin stylesheet.
     */
    public function enqueue_admin_styles()
    {
        wp_enqueue_style($this->plugin_slug . '-admin-styles', plugins_url('assets/css/admin.css', __FILE__), array(), Headlines::VERSION);
        wp_enqueue_style($this->plugin_slug . '-select2-styles', plugins_url('assets/css/select2.css', __FILE__), array(), Headlines::VERSION);
    }

    /**
     * Register and enqueue admin Javascript files.
     */
    public function enqueue_admin_scripts()
    {
        $screen = get_current_screen();
        if (in_array($screen->id, array('post', 'page'))) {
            wp_enqueue_script($this->plugin_slug . '-admin-script', plugins_url('assets/js/admin.js', __FILE__), array('jquery'), Headlines::VERSION);
        }

        if (in_array($screen->id, array('toplevel_page_ks-headlines'))) {
            wp_enqueue_script($this->plugin_slug . '-select2-script', plugins_url('assets/js/select2.min.js', __FILE__), array('jquery'), Headlines::VERSION);
        }
    }
	//autocomplete posts if you take a while to save
    public function autocomplete_posts()
    {
        $q = $_REQUEST['q'];
        $page = (int) $_REQUEST['page'];
        $page_limit = (int) $_REQUEST['page_limit'];

        if (!$page) $page = 1;
        if (!$page_limit) $page_limit = 10;

        $ret = array(
            'total' => 0,
            'posts' => array()
        );

        $args = array(
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => array('page','post'),
            'suppress_filters' => true,
            's' => $q,
            'meta_query' => array(
                array(
                    'key' => 'headlines_post_guid',
                    'compare' => 'IS NOT NULL'
                )
            )
        );

        $posts = get_posts($args);
        $ret['total'] = count($posts);

        $args['posts_per_page'] = $page_limit;
        $args['offset'] = ($page - 1) * $page_limit;
        $posts = get_posts($args);

        foreach ($posts as $post) {
            $post_guid = get_post_meta($post->ID, 'headlines_post_guid', true);
            $post_title = $post->post_title;

            $ret['posts'][] = array(
                'id' => $post->ID,
                'text' => $post_title
            );
        }

        header('Content-Type: text/json');

        echo json_encode($ret);
        exit;
    }
	
	//loads the posts variations
    public function load_post_variations()
    {
        $post_id = $_REQUEST['post_id'];

        $baseline_rate = 0;

        $baseline = Headlines_DB::get_post_baseline_variation($post_id, OBJECT);
        if ($baseline) {
            @$baseline_rate = (float) ($baseline->viral_count / $baseline->visit_count);
        }

        $rows = Headlines_DB::get_post_variations($post_id, ARRAY_A);
        foreach ($rows as &$row) {
            @$rate = (float) ($row['viral_count'] / $row['visit_count']);

       	    @$delta = $baseline_rate > 0 ? (($rate / $baseline_rate) - 1) * 100 : $rate * 100;

            $row['conversion_rate'] = $rate;
            $row['conversion_delta'] = $delta;
        }

        header('Content-Type: text/json');

        echo json_encode($rows);
        exit;
    }
	
	//save post variations when you "publish"
    public function save_post_variations($post_id)
    {
        if (wp_is_post_revision($post_id)) {
            return $post_id;
        }

        $post = get_post($post_id);
        if ($post->post_status == 'trash' or $post->post_status == 'auto-draft') {
            return $post_id;
        }

        if (!in_array(get_post_type($post_id), array('post', 'page'))) {
            return $post_id;
        }

		    $primary = Headlines_DB::get_primary_post_variation($post_id, OBJECT);
        $post_title_variations = array(array(
            'id' => $primary ? $primary->ID : null,
            'title' => $_POST['post_title']
        ));

        // no variations sent
        if (!isset($_POST['post_title_variation']) || empty($_POST['post_title_variation'])) {
            // and no previous entries
            $count = Headlines_DB::get_num_post_variations($post_id, true);
            if ($count == 0) {
                // skip out, do nothing
                return;
            }
        }
		
        if (isset($_POST['post_title_variation'])) {
            $post_title_variations = array_merge($post_title_variations, $_POST['post_title_variation']);
        }

        $ids = array();
        $pos = 1;
        foreach ($post_title_variations as $index => $variation) {
            $post_title = stripslashes_deep(trim($variation['title']));
            if (!$post_title) {
                continue;
            }

            $id = $variation['id'];

            if ($id) {
                $id = Headlines_DB::update_post_variation($id, $post_title, $pos, ($index == 0));
            } else {
                $id = Headlines_DB::add_post_variation($post_id, $post_title, $pos, ($index == 0));
            }

            $ids[] = $id;
            $pos++;
        }

        Headlines_DB::delete_post_variations($post_id, $ids);
    }

    public function admin_notices()
    {
        $url = menu_page_url('headlines-options', false);

        if (!trim(get_option(KS_HEADLINES_OPTION_LICENSE_KEY))) {
          echo <<<EOF
<div class="error ks-license-error"><p>
<strong>KingSumo Headlines</strong> does not have a license key yet.
&nbsp;
<a href="{$url}">Click here</a> to enter your license key (Settings &#8594; KingSumo Headlines)
</p></div>
EOF;
        } else if (get_option(KS_HEADLINES_OPTION_LICENSE_STATUS) != 'valid') {
          echo <<<EOF
<div class="error ks-license-error"><p>
<strong>KingSumo Headlines</strong> has not been activated yet.
&nbsp;
<a href="{$url}">Click here</a> to activate your license (Settings &#8594; KingSumo Headlines)
</p></div>
EOF;
        }
    }
}
